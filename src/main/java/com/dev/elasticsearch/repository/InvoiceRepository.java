package com.dev.elasticsearch.repository;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.Result;
import co.elastic.clients.elasticsearch.core.*;
import co.elastic.clients.elasticsearch.core.search.Hit;
import com.dev.elasticsearch.entity.Invoice;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class InvoiceRepository {

    private final ElasticsearchClient elasticsearchClient;

    private final String indexName = "invoices";

    public InvoiceRepository(ElasticsearchClient elasticsearchClient) {
        this.elasticsearchClient = elasticsearchClient;
    }

    public String createOrUpdateInvoice(Invoice invoice) throws IOException {
        IndexResponse response = this.elasticsearchClient.index(
                i -> i.index(this.indexName).id(invoice.getId()).document(invoice)
        );

        String name = response.result().name();

        if (name.equals("Created")) {
            return "Invoice document has been created successfully.";
        }

        if (name.equals("Updated")) {
            return "Invoice document has been updated successfully.";
        }

        return "Error while performing the operation.";
    }

    public Invoice getInvoiceById(String invoiceId) throws IOException {
        GetResponse<Invoice> response = this.elasticsearchClient.get(
                g -> g.index(this.indexName).id(invoiceId), Invoice.class
        );

        if (response.found()) {
            Invoice invoice = response.source();

            if (null != invoice) {
                System.out.printf("Invoice name is: %s", invoice.getName());
            }
            return invoice;
        }

        System.out.println("Invoice not found");
        return null;
    }

    public String deleteInvoiceById(String invoiceId) throws IOException {
        DeleteRequest request = DeleteRequest.of(d -> d.index(this.indexName).id(invoiceId));
        DeleteResponse deleteResponse = this.elasticsearchClient.delete(request);

        String id = deleteResponse.id();
        Result result = deleteResponse.result();
        if (Objects.nonNull(result) && !result.name().equals("NotFound")) {
            return String.format("Invoice with id : %s has been deleted successfully !.", id);
        }

        System.out.println("Invoice not found");
        return String.format("Invoice with id : %s does not exist.", id);
    }

    public List<Invoice> getAllInvoices() throws IOException {
        SearchRequest searchRequest = SearchRequest.of(s -> s.index(this.indexName));
        SearchResponse<Invoice> searchResponse = this.elasticsearchClient.search(searchRequest, Invoice.class);
        List<Hit<Invoice>> hits = searchResponse.hits().hits();
        List<Invoice> invoices = new ArrayList<>();
        for (Hit<Invoice> object : hits) {
            System.out.print(object.source());
            invoices.add(object.source());
        }
        return invoices;
    }
}
