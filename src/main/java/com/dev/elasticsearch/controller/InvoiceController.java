package com.dev.elasticsearch.controller;

import com.dev.elasticsearch.entity.Invoice;
import com.dev.elasticsearch.repository.InvoiceRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/invoice")
public class InvoiceController {
    private final InvoiceRepository invoiceRepository;

    public InvoiceController(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @PostMapping("/create-or-update")
    public ResponseEntity<String> createOrUpdateInvoice(@RequestBody Invoice invoice) throws IOException {
        String response = this.invoiceRepository.createOrUpdateInvoice(invoice);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/get-by-id")
    public ResponseEntity<Invoice> getInvoiceById(@RequestParam String invoiceId) throws IOException {
        Invoice invoice = this.invoiceRepository.getInvoiceById(invoiceId);
        return ResponseEntity.ok(invoice);
    }

    @GetMapping("/get-all")
    public ResponseEntity<List<Invoice>> getAllInvoices() throws IOException {
        List<Invoice> invoices = this.invoiceRepository.getAllInvoices();
        return ResponseEntity.ok(invoices);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteInvoiceById(@RequestParam String invoiceId) throws IOException {
        String response = this.invoiceRepository.deleteInvoiceById(invoiceId);
        return ResponseEntity.ok(response);
    }
}
